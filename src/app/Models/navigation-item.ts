export class NavigationItem {
    pageId: number;
    url: string;
    name: string;
    type: string;
    logo: string;
    intro: string;
    children: NavigationItem[];
}
