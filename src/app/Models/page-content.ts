export class PageContent {
    title: string;
    subTitle: string;
    headerImage: string;
    content: string;
}
