import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavigationItem } from '../../Models/navigation-item';
import { NavigationService } from '../../Services/Navigation/navigation.service';
import { PageContent } from '../../Models/page-content';

@Component({
  selector: 'app-sub-area',
  templateUrl: './sub-area.component.html',
  styleUrls: ['./sub-area.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubAreaComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router) { }

  ngOnInit() {
    this.getNavigation();
  }

  categoryItems: NavigationItem[];
  areaItems: NavigationItem[];
  subAreaItems: NavigationItem[];
  childAreaItems: NavigationItem[];
  subAreaContent: PageContent;
  selectedArea: NavigationItem;
  loading: boolean;

  getNavigation() {
    this.loading = true;

    // Check the cache and if empty get the navigation from the API
    let key = 'navigation';
    if(sessionStorage.getItem(key) !== null) {
      this.categoryItems = JSON.parse(sessionStorage.getItem(key));
      this.getChildAreaItems();
    } else {
      this.getNavigationFromApi(key);
    }
  }

  getNavigationFromApi(key: string) {
    this.navigationService.getNavigation().subscribe(
      data => {
        this.categoryItems = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryItems));
        this.getChildAreaItems();
      }
    );
  }

  getChildAreaItems() {
    this.activatedRoute.params.subscribe((params: Params) => {

      // Get selected category and it's children
      let catId = params['catId'];
      let selectedCategory =  this.navigationService.getSelectedNavigationItem(this.categoryItems, catId);
      if(!selectedCategory) {
        this.router.navigate(['/404']);
        this.loading = false;
        return;
      }
      this.areaItems = selectedCategory.children;

      // Get selected area and it's children
      let areaId = params['areaId'];
      this.selectedArea = this.navigationService.getSelectedNavigationItem(this.areaItems, areaId);
      if(!this.selectedArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }
      this.subAreaItems = this.selectedArea.children;

      // Get selected sub-area and it's children
      let subAreaId = params['subAreaId'];
      let selectedSubArea = this.navigationService.getSelectedNavigationItem(this.subAreaItems, subAreaId);
      if(!selectedSubArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }
      this.childAreaItems = selectedSubArea.children;

      // Check the cache and if empty get the page content from the API
      let key = 'subarea-' + subAreaId;
      if(sessionStorage.getItem(key) !== null) {
        this.subAreaContent = JSON.parse(sessionStorage.getItem(key));
        this.loading = false;
      } else {
        this.navigationService.getPageContent(subAreaId, 'subarea').subscribe(
          data => {
            this.subAreaContent = data;
            sessionStorage.setItem(key, JSON.stringify(this.subAreaContent));
            this.loading = false;
          },
          error => {
            console.log(error);
          }
        );
      }
    });
  }

}
