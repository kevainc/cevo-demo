import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationService } from '../../Services/Navigation/navigation.service';
import { MockNavigationService } from '../../Services/Navigation/mock-navigation-service';
import { Observable } from 'rxjs/Observable';
import { HttpModule } from "@angular/http";

import { SubAreaComponent } from './sub-area.component';

describe('SubAreaComponent', () => {
  let component: SubAreaComponent;
  let fixture: ComponentFixture<SubAreaComponent>;
  let navigationService: NavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: NavigationService, useClass: MockNavigationService}],
      imports: [RouterTestingModule, HttpModule],
      declarations: [ SubAreaComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
    navigationService = TestBed.get(NavigationService);
    fixture = TestBed.createComponent(SubAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
