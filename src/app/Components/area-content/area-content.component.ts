import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { PageContent } from '../../Models/page-content';
import { NavigationItem } from '../../Models/navigation-item';

@Component({
  selector: 'app-area-content',
  templateUrl: './area-content.component.html',
  styleUrls: ['./area-content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AreaContentComponent implements OnInit {

  @Input()
  areaContent: PageContent;

  @Input()
  subAreaItems: NavigationItem[]

  constructor() { }

  ngOnInit() {
  }

}
