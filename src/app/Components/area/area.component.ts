import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavigationItem } from '../../Models/navigation-item';
import { NavigationService } from '../../Services/Navigation/navigation.service';
import { PageContent } from '../../Models/page-content';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AreaComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, 
    private navigationService: NavigationService, 
    private router: Router) { }

  ngOnInit() {
    this.getNavigation();
  }

  categoryItems: NavigationItem[];
  areaItems: NavigationItem[];
  subAreaItems: NavigationItem[];
  areaContent: PageContent;
  loading: boolean;

  getNavigation() {
    this.loading = true;

    // Check the cache and if empty get the navigation from the API
    let key = 'navigation';
    if(sessionStorage.getItem(key) !== null) {
      this.categoryItems = JSON.parse(sessionStorage.getItem(key));
      this.getSubAreaItems();
    } else {
      this.getNavigationFromApi(key);
    }
  }

  getNavigationFromApi(key: string) {
    this.navigationService.getNavigation().subscribe(
      data => {
        this.categoryItems = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryItems));
        this.getSubAreaItems();
      }
    );
  }

  getSubAreaItems() {
    this.activatedRoute.params.subscribe((params: Params) => {

      // Get selected category and it's children
      let catId = params['catId'];
      let selectedCategory =  this.navigationService.getSelectedNavigationItem(this.categoryItems, catId);
      if(!selectedCategory) {
        this.router.navigate(['/404']);
        this.loading = false;
        return;
      }
      this.areaItems = selectedCategory.children;

      // Get selected area and it's children
      let areaId = params['areaId'];
      let selectedArea = this.navigationService.getSelectedNavigationItem(this.areaItems, areaId);
      if(!selectedArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }
      this.subAreaItems = selectedArea.children;

      // Check the cache and if empty get the page content from the API
      let key = 'area-' + areaId;
      if(sessionStorage.getItem(key) !== null) {
        this.areaContent = JSON.parse(sessionStorage.getItem(key));
        this.loading = false;
      } else {
        this.getAreaContent(areaId, key);
      }
    });
  }

  getAreaContent(areaId: number, key: string) {
    this.navigationService.getPageContent(areaId, 'area').subscribe(
      data => {
        this.areaContent = data;
        sessionStorage.setItem(key, JSON.stringify(this.areaContent));
        this.loading = false;
      }
    );
  }

}
