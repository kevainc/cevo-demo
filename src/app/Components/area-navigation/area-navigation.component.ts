import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NavigationItem } from '../../Models/navigation-item';

@Component({
  selector: 'app-area-navigation',
  templateUrl: './area-navigation.component.html',
  styleUrls: ['./area-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AreaNavigationComponent implements OnInit {

  @Input()
  areaItems: NavigationItem[];

  listedAreaItems;
  colSize = 12;

  constructor() { }

  ngOnInit() {
    if(this.areaItems) {
      this.listedAreaItems = this.splitArrayInPieces(this.areaItems, 10);
      this.colSize = Math.round(12/this.listedAreaItems.length);
    }
  }

  splitArrayInPieces(arrayToSplit, chunkSize) {
    return [].concat.apply([], 
      arrayToSplit.map((elim, i) => {
        return i%chunkSize ? [] : [arrayToSplit.slice(i,i+chunkSize)]
      }));
  }

}
