import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavigationItem } from '../../Models/navigation-item';
import { PageContent } from '../../Models/page-content';
import { NavigationService } from '../../Services/Navigation/navigation.service';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryComponent implements OnInit {

  categoryItems: NavigationItem[];
  areaItems: NavigationItem[];
  loading: boolean;
  categoryContent: PageContent; 

  constructor(private activatedRoute: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router) { }

  ngOnInit() {
    this.getNavigation();
  }

  getNavigation() {
    this.loading = true;

    // Check the cache and if empty get the navigation from the API
    let key = 'navigation';
    if(sessionStorage.getItem(key) !== null) {
      this.categoryItems = JSON.parse(sessionStorage.getItem(key));
      this.getCategoryContent();
    } else {
      this.getNavigationFromApi(key);
    }
  }

  getNavigationFromApi(key: string) {
    this.navigationService.getNavigation().subscribe(
      data => {
        this.categoryItems = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryItems));
        this.getCategoryContent();
      }
    );
  }

  getCategoryContent() {
    this.activatedRoute.params.subscribe((params: Params) => {
      
      // Get selected category to make sure the page exists
      let catId = params['catId'];
      let selectedCategory = this.navigationService.getSelectedNavigationItem(this.categoryItems, catId);
      if(!selectedCategory) {
        this.router.navigate(['/404']);
        this.loading = false;
        return;
      }

      // Check the cache and if empty get the page content from the API
      let key = 'category-' + catId;
      if(sessionStorage.getItem(key) !== null) {
        this.categoryContent = JSON.parse(sessionStorage.getItem(key));
        this.loading = false;
      } else {
        this.getCategoryContentFromApi(catId, key);
      }
    });
  }

  getCategoryContentFromApi(catId: number, key: string) {
    this.navigationService.getPageContent(catId, 'category').subscribe(
      data => {
        this.categoryContent = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryContent));
        this.loading = false;
      }
    );
  }
}
