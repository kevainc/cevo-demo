import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NavigationItem } from '../../Models/navigation-item';

@Component({
  selector: 'app-category-navigation',
  templateUrl: './category-navigation.component.html',
  styleUrls: ['./category-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryNavigationComponent implements OnInit {

  @Input()
  categoryItems: NavigationItem[];

  constructor() { }

  ngOnInit() {
  }

}
