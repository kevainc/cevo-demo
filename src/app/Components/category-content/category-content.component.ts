import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { PageContent } from '../../Models/page-content';

@Component({
  selector: 'app-category-content',
  templateUrl: './category-content.component.html',
  styleUrls: ['./category-content.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryContentComponent implements OnInit {

  @Input()
  categoryContent: PageContent;

  constructor() { }

  ngOnInit() {
  }

}
