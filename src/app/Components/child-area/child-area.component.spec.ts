import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationService } from '../../Services/Navigation/navigation.service';
import { MockNavigationService } from '../../Services/Navigation/mock-navigation-service';
import { Observable } from 'rxjs/Observable';
import { HttpModule } from "@angular/http";

import { ChildAreaComponent } from './child-area.component';

describe('ChildAreaComponent', () => {
  let component: ChildAreaComponent;
  let fixture: ComponentFixture<ChildAreaComponent>;
  let navigationService: NavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: NavigationService, useClass: MockNavigationService}],
      imports: [RouterTestingModule, HttpModule],
      declarations: [ ChildAreaComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
    navigationService = TestBed.get(NavigationService);
    fixture = TestBed.createComponent(ChildAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
