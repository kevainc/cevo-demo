import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavigationItem } from '../../Models/navigation-item';
import { NavigationService } from '../../Services/Navigation/navigation.service';
import { PageContent } from '../../Models/page-content';

@Component({
  selector: 'app-child-area',
  templateUrl: './child-area.component.html',
  styleUrls: ['./child-area.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChildAreaComponent implements OnInit {

  categoryItems: NavigationItem[];
  areaItems: NavigationItem[];
  subAreaItems: NavigationItem[];
  childAreaItems: NavigationItem[];
  childAreaContent: PageContent;
  selectedSubArea: NavigationItem;
  loading: boolean;

  constructor(private activatedRoute: ActivatedRoute,
    private navigationService: NavigationService,
    private router: Router) { }

  ngOnInit() {
    this.getNavigation();
  }

  getNavigation() {
    this.loading = true;

    // Check the cache and if empty get the navigation from the API
    let key = 'navigation';
    if(sessionStorage.getItem(key) !== null) {
      this.categoryItems = JSON.parse(sessionStorage.getItem(key));
      this.getChildAreaItemInfo();
    } else {
      this.getNavigationFromApi(key);
    }
  }

  getNavigationFromApi(key: string) {
    this.navigationService.getNavigation().subscribe(
      data => {
        this.categoryItems = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryItems));
        this.getChildAreaItemInfo();
      }
    );
  }

  getChildAreaItemInfo() {
    this.activatedRoute.params.subscribe((params: Params) => {

      // Get selected category and it's children
      let catId = params['catId'];
      let selectedCategory =  this.navigationService.getSelectedNavigationItem(this.categoryItems, catId);
      if(!selectedCategory) {
        this.router.navigate(['/404']);
        this.loading = false;
        return;
      }
      this.areaItems = selectedCategory.children;

      // Get selected area and it's children
      let areaId = params['areaId'];
      let selectedArea = this.navigationService.getSelectedNavigationItem(this.areaItems, areaId);
      if(!selectedArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }
      this.subAreaItems = selectedArea.children;

      // Get selected sub-area and it's children
      let subAreaId = params['subAreaId'];
      this.selectedSubArea = this.navigationService.getSelectedNavigationItem(this.subAreaItems, subAreaId);
      if(!this.selectedSubArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }
      this.childAreaItems = this.selectedSubArea.children;

      // Get selected child-area
      let childAreaId = params['childAreaId'];
      let selectedChildArea = this.navigationService.getSelectedNavigationItem(this.childAreaItems, childAreaId);
      if(!selectedChildArea) {
        this.router.navigate(['/404'], {});
        this.loading = false;
        return;
      }

      // Check the cache and if empty get the page content from the API
      let key = 'childarea-' + subAreaId;
      if(sessionStorage.getItem(key) !== null) {
        this.childAreaContent = JSON.parse(sessionStorage.getItem(key));
        this.loading = false;
      } else {
        this.navigationService.getPageContent(subAreaId, 'childarea').subscribe(
          data => {
            this.childAreaContent = data;
            sessionStorage.setItem(key, JSON.stringify(this.childAreaContent));
            this.loading = false;
          },
          error => {
            console.log(error);
          }
        );
      }
    });
  }

}
