import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NavigationItem } from '../../Models/navigation-item';

@Component({
  selector: 'app-child-area-navigation',
  templateUrl: './child-area-navigation.component.html',
  styleUrls: ['./child-area-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChildAreaNavigationComponent implements OnInit {

  @Input()
  childAreaItem: NavigationItem;

  constructor() { }

  ngOnInit() {
  }

}
