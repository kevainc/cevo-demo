import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NavigationItem } from '../../Models/navigation-item';

@Component({
  selector: 'app-sub-area-navigation',
  templateUrl: './sub-area-navigation.component.html',
  styleUrls: ['./sub-area-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubAreaNavigationComponent implements OnInit {

  @Input()
  subAreaItems: NavigationItem[];

  constructor() { }

  ngOnInit() {
  }

}
