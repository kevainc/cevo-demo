import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CategoryComponent } from './Components/category/category.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { LoadingComponent } from './Components/loading/loading.component';
import { CategoryNavigationComponent } from './Components/category-navigation/category-navigation.component';
import { AreaComponent } from './Components/area/area.component';
import { SubAreaNavigationComponent } from './Components/sub-area-navigation/sub-area-navigation.component';
import { SubAreaComponent } from './Components/sub-area/sub-area.component';
import { CategoryContentComponent } from './Components/category-content/category-content.component';
import { AreaContentComponent } from './Components/area-content/area-content.component';
import { AreaNavigationComponent } from './Components/area-navigation/area-navigation.component';
import { ChildAreaNavigationComponent } from './Components/child-area-navigation/child-area-navigation.component';
import { ChildAreaComponent } from './Components/child-area/child-area.component';

import { NavigationService } from './Services/Navigation/navigation.service';

const appRoutes: Routes = [
  { path: 'category/:catId', component: CategoryComponent, },
  { path: 'category/:catId/area/:areaId', component: AreaComponent},
  { path: 'category/:catId/area/:areaId/subarea/:subAreaId', component: SubAreaComponent},
  { path: 'category/:catId/area/:areaId/subarea/:subAreaId/childarea/:childAreaId', component: ChildAreaComponent},
  { path: '',
    redirectTo: '/category/1',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    CategoryComponent,
    PageNotFoundComponent,
    LoadingComponent,
    CategoryNavigationComponent,
    AreaComponent,
    SubAreaNavigationComponent,
    SubAreaComponent,
    CategoryContentComponent,
    AreaContentComponent,
    AreaNavigationComponent,
    ChildAreaNavigationComponent,
    ChildAreaComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  providers: [NavigationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
