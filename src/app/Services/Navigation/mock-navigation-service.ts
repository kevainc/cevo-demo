import { NavigationItem } from '../../Models/navigation-item';
import { PageContent } from '../../Models/page-content';
import { Observable } from 'rxjs/Observable';

export class MockNavigationService {
    private createNavItems(): NavigationItem {
        let navItem1 = new NavigationItem();
        let navItem2 = new NavigationItem();
        let navItem3 = new NavigationItem();
        let navItem4 = new NavigationItem();

        navItem1.pageId = 1;
        navItem1.intro = 'intro text';
        navItem1.name = 'nav item 1';
        navItem1.url = '/category/1';
        navItem1.type = 'Category';
        navItem1.children = [navItem2];

        navItem2.pageId = 2;
        navItem2.intro = 'intro text';
        navItem2.name = 'nav item 2';
        navItem2.url = '/category/1/area/2';
        navItem2.type = 'Area';
        navItem2.children = [navItem3];

        navItem3.pageId = 3;
        navItem3.intro = 'intro text';
        navItem3.name = 'nav item 3';
        navItem3.url = '/category/1/area/2/subarea/3';
        navItem3.type = 'Subarea';
        navItem3.children = [navItem4];

        navItem4.pageId = 4;
        navItem4.intro = 'intro text';
        navItem4.name = 'nav item 4';
        navItem4.url = '/category/1/area/2subarea/3/childarea/4';
        navItem4.type = 'Childarea';
        navItem4.children = null;

        return navItem1;
    }

    public getNavigation(): Observable<NavigationItem[]> {
        let navItem = this.createNavItems();
        return Observable.of([navItem]);
    }

    public getSelectedNavigationItem(): NavigationItem {
        let navItem = this.createNavItems();
        return navItem;
    }

    public getPageContent(): Observable<PageContent> {
        let pageContent = new PageContent();
        pageContent.content = "test content";
        pageContent.headerImage = "null";
        pageContent.subTitle = "subtitle";
        pageContent.title = "title";
        return Observable.of(pageContent);
    }
}