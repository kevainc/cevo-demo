import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { NavigationItem } from '../../Models/navigation-item';
import { PageContent } from '../../Models/page-content';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/retry';

@Injectable()
export class NavigationService {
  private http;
  private apiUrlBase = "http://private-4641ea-cevodemo.apiary-mock.com";
  
  constructor(http: Http) { 
    this.http = http;
  }

  getNavigation(): Observable<NavigationItem[]> {
    let model: NavigationItem[];

    return this.http.get(this.apiUrlBase + '/navigation')
            .retry(5)
            .map(result => {
              model = result.json() as NavigationItem[];
              return Array.from(model);
            })
            .catch(error => console.log('Could not load data in service for getting the navigation.'));       
  }

  getSelectedNavigationItem(parentNavigationItems: NavigationItem[], id: any): NavigationItem {
    if(id && parentNavigationItems) {
      id = Number(id);
      let selectedNavigationItem = parentNavigationItems.find(item => {return item.pageId == id});
      if(selectedNavigationItem) {
        return selectedNavigationItem;
      }
    }
    return null;
  }

  getPageContent(pageId: Number, type: string): Observable<PageContent> {
    let model: PageContent;

    return this.http.get(this.apiUrlBase + '/' + type + '/' + pageId)
    .map(result => {
      model = result.json() as PageContent;
      return model;
    })
    .catch(error => console.log('Could not load data in service for getting the navigation.'));
  }
}
