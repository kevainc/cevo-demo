import { Component, OnInit } from '@angular/core';
import { NavigationItem } from './Models/navigation-item';
import { NavigationService } from './Services/Navigation/navigation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  categoryItems: NavigationItem[];
  loading: Boolean;
  constructor(private navigationService: NavigationService) { }
  
  ngOnInit(): void {
    this.getNavigation();
  }

  getNavigation() {
    this.loading = true;

    // Check the cache and if empty get the navigation from the API
    let key = 'navigation';
    if(sessionStorage.getItem(key) !== null) {
      this.categoryItems = JSON.parse(sessionStorage.getItem(key));
      this.loading = false;
    } else {
      this.getNavigationFromApi(key);
    }
  }

  getNavigationFromApi(key: string) {
    this.navigationService.getNavigation().subscribe(
      data => {
        this.categoryItems = data;
        sessionStorage.setItem(key, JSON.stringify(this.categoryItems));
        this.loading = false;
      }
    );
  }
}
