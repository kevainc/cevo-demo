# CevoDemo

## Task

We’d like you to implement a service that requests the navigation
structure, the template code to render the navigation, a basic level of styling, and the view
navigation.

## Deliverables
* Mock JSON data of the navigation
* The navigation service
* Application routes
* HTML templates
* SASS file
* Tests

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).